//== Class definition

var FormWidgets = function () {
    //== Private functions
    var validator;
    // bootstrap select
    var initWidgets = function () {

        // select2
        $('#role').select2({
            placeholder: "Выберите",

        });

        $('#position').select2({
            placeholder: "Выберите",

        });


    }

    var initValidator = function () {
        validator = $("#m_form_1").validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 6
                },
                name: {
                    required: true
                },
                surname: {
                    required: true
                },
                phone: {
                    required: true,
                    number: true
                },
                birthday: {
                    required: true,
                    date: true
                },

                role: {
                    required: true
                },
                position: {
                    required: true
                }
            },
            messages: {
                email: {
                    required: "Поле обязательно для заполнения",
                    email: "Укажите корректный email"
                    // minlength: "Введите минимум 6 символов",
                },
                name: {
                    required: "Поле обязательно для заполнения"
                },
                surname: {
                    required: "Поле обязательно для заполнения"
                },
                phone: {
                    required: "Поле обязательно для заполнения",
                    number: "Укажите корректный номер телефона"
                },
                birthday: {
                    required: "Поле обязательно для заполнения",
                    date: "Укажите корректную дату"
                },
                role: {
                    required: "Необходимо выбрать роль"
                },
                position: {
                    required: "Необходимо выбрать должность"
                },
                password: {
                    required: "Поле обязательно для заполнения",
                    minlength: "Введите минимум 6 символов"
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#m_form_1_msg');
                alert.removeClass('m--hide').show();
                mApp.scrollTo(alert, -200);
            },

            submitHandler: function (form) {

                form[0].submit(); // submit the form
            }
        });
    }

    return {
        // public functions
        init: function () {
            initWidgets();
            initValidator();
        }
    };
}();

jQuery(document).ready(function () {
    FormWidgets.init();
});