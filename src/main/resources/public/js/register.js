//== Class Definition
var SnippetLogin = function () {

    var login = $('#m_login');

    // $('#position').select2({
    //     placeholder: "Выберите роль",
    //
    // });

    var showErrorMsg = function (form, type, msg) {
        var alert = $('<div class="m-alert m-alert--outline alert alert-' + type + ' alert-dismissible" role="alert">\
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
			<span></span>\
		</div>');

        form.find('.alert').remove();
        alert.prependTo(form);
        alert.animateClass('fadeIn animated');
        alert.find('span').html(msg);
    }

    //== Private Functions

    var displaySignUpForm = function () {
        login.removeClass('m-login--forget-password');
        login.removeClass('m-login--signin');

        login.addClass('m-login--signup');
        login.find('.m-login__signup').animateClass('flipInX animated');
    }

    var displaySignInForm = function () {
        login.removeClass('m-login--forget-password');
        login.removeClass('m-login--signup');

        login.addClass('m-login--signin');
        login.find('.m-login__signin').animateClass('flipInX animated');
    }

    var displayForgetPasswordForm = function () {
        login.removeClass('m-login--signin');
        login.removeClass('m-login--signup');

        login.addClass('m-login--forget-password');
        login.find('.m-login__forget-password').animateClass('flipInX animated');
    }

    var handleFormSwitch = function () {
        // $('#m_login_forget_password').click(function (e) {
        //     e.preventDefault();
        //     displayForgetPasswordForm();
        // });
        //
        // $('#m_login_forget_password_cancel').click(function (e) {
        //     e.preventDefault();
        //     displaySignInForm();
        // });

        $('#m_login_signup').click(function (e) {
            e.preventDefault();

            displaySignUpForm();
        });

        $('#m_login_signup_cancel').click(function (e) {
            e.preventDefault();
            displaySignInForm();
        });
    }


    var handleSignUpFormSubmit = function () {
        $('#m_login_signup_submit').click(function (e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    name: {
                        required: true
                    },
                    surname: {
                        required: true
                    },
                    phone: {
                        required: true,
                        number: true
                    },
                    birthday: {
                        required: true,
                        date: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                        minlength: 6
                    },
                    rpassword: {
                        required: true,
                        equalTo: "#regPass"
                    },
                    position: {
                        required: true
                    }
                },
                messages: {
                    name: {
                        required: "Поле обязательно для заполнения"
                    },
                    surname: {
                        required: "Поле обязательно для заполнения"
                    },
                    phone: {
                        required: "Поле обязательно для заполнения",
                        number: "Неправильный формат номера"
                    },
                    birthday: {
                        required: "Поле обязательно для заполнения",
                        date: "Неправильный формат даты"
                    },
                    email: {
                        required: "Поле обязательно для заполнения",
                        email: "Неправильный формат почты"
                    },
                    password: {
                        required: "Поле обязательно для заполнения",
                        minlength: "Парль должен содержать миниму 6 символов"
                    },
                    rpassword: {
                        required: "Поле обязательно для заполнения",
                        equalTo: "Пароли не совпадают"

                    },
                    position: {
                        required: "Поле обязательно для заполнения"
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
            form.submit();
            //
            // todo тут какая то каша из фронта и бека надо будет разобрать ее

            // form.ajaxSubmit({
            //     url: '/registration',
            //     type: "POST",
            //     success: function (response, status, xhr, $form) {
            //         window.location = "/login";
            //         //todo successful register message
            //
            //         // similate 2s delay
            //         setTimeout(function () {
            //             btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
            //             form.clearForm();
            //             form.validate().resetForm();
            //
            //             // display signup form
            //             displaySignInForm();
            //             var signInForm = login.find('.m-login__signin form');
            //             signInForm.clearForm();
            //             signInForm.validate().resetForm();
            //
            //             showErrorMsg(signInForm, 'success', 'Thank you. To complete your registration please check your email.');
            //         }, 2000);
            //     }
            // });
        });
    }

    //== Public Functions
    return {
        // public functions
        init: function () {
            handleSignUpFormSubmit();
            handleFormSwitch();
        }
    };
}();

//== Class Initialization
jQuery(document).ready(function () {
    SnippetLogin.init();
});