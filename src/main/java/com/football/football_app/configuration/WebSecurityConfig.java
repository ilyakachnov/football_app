package com.football.football_app.configuration;

import com.football.football_app.security.AppUserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private AppUserDetailsServiceImpl applUserDetailsService;

    @Autowired
    public WebSecurityConfig(AppUserDetailsServiceImpl applUserDetailsService) {
        this.applUserDetailsService = applUserDetailsService;
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/css/**", "/js/**", "/img/**", "/registration", "/forgot", "/reset").permitAll()
                .antMatchers("/teams/{\\d+}/players**").authenticated()
                .antMatchers("/users/**").hasAuthority("ADMIN_ACTIVE")
                .antMatchers("/teams/**").hasAnyAuthority("ADMIN_ACTIVE", "MANAGER_ACTIVE")
                .antMatchers("/tournaments/**").hasAnyAuthority("ADMIN_ACTIVE", "ORGANIZER_ACTIVE")
                .antMatchers("/leagues/**").hasAnyAuthority("ADMIN_ACTIVE")
                .antMatchers("/stadiums/**").hasAnyAuthority("ADMIN_ACTIVE")
                .antMatchers("/profile/**").hasAnyAuthority("ADMIN_ACTIVE", "MANAGER_ACTIVE", "ORGANIZER_ACTIVE", "PLAYER_ACTIVE")
                .antMatchers("/allteams").hasAuthority("PLAYER_ACTIVE")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .permitAll()
                .and()
                .logout()
                .permitAll();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(applUserDetailsService).passwordEncoder(bCryptPasswordEncoder());
    }
}
