package com.football.football_app.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

@Configuration
@ConfigurationProperties(prefix = "msg-template-text")
@Validated
public class MsgTemplateConfigurationProperties {

    @NotBlank
    private String newUserNotification;

    @NotBlank
    private String accountActivation;

    @NotBlank
    private String newUserMsgSubject;

    @NotBlank
    private String passwordResetSubject;

    @NotBlank
    private String passwordResetMessage;

    public String getNewUserNotification() {
        return newUserNotification;
    }

    public void setNewUserNotification(String newUserNotification) {
        this.newUserNotification = newUserNotification;
    }

    public String getAccountActivation() {
        return accountActivation;
    }

    public void setAccountActivation(String accountActivation) {
        this.accountActivation = accountActivation;
    }

    public String getNewUserMsgSubject() {
        return newUserMsgSubject;
    }

    public void setNewUserMsgSubject(String newUserMsgSubject) {
        this.newUserMsgSubject = newUserMsgSubject;
    }


    public String getPasswordResetMessage() {
        return passwordResetMessage;
    }

    public void setPasswordResetMessage(String passwordResetMessage) {
        this.passwordResetMessage = passwordResetMessage;
    }

    public String getPasswordResetSubject() {
        return passwordResetSubject;
    }

    public void setPasswordResetSubject(String passwordResetSubject) {
        this.passwordResetSubject = passwordResetSubject;
    }
}
