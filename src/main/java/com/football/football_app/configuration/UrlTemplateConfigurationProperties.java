package com.football.football_app.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

@Configuration
@ConfigurationProperties(prefix = "url-template")
@Validated
public class UrlTemplateConfigurationProperties {

    @NotBlank
    private String userProfileUrl;

    @NotBlank
    private String userActivationUrl;

    @NotBlank
    private String resetPasswordUrl;

    public String getUserProfileUrl() {
        return userProfileUrl;
    }

    public void setUserProfileUrl(String userProfileUrl) {
        this.userProfileUrl = userProfileUrl;
    }

    public String getUserActivationUrl() {
        return userActivationUrl;
    }

    public void setUserActivationUrl(String userActivationUrl) {
        this.userActivationUrl = userActivationUrl;
    }

    public String getResetPasswordUrl() {
        return resetPasswordUrl;
    }

    public void setResetPasswordUrl(String resetPasswordUrl) {
        this.resetPasswordUrl = resetPasswordUrl;
    }
}
