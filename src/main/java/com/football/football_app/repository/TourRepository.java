package com.football.football_app.repository;

import com.football.football_app.model.AppUser;
import com.football.football_app.model.Tour;
import com.football.football_app.model.Tournament;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface TourRepository extends JpaRepository<Tour, Long> {
    List<Tour> findAllByTournament(Tournament tournament);

    @Query("select t from Tour t inner join t.tournament as tt where t.id =:id and tt.organizer =:organizer")
    Optional<Tour> findTourByOrganizer(@Param("id") Long id, @Param("organizer") AppUser organizer);

}
