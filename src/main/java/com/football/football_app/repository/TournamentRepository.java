package com.football.football_app.repository;

import com.football.football_app.model.AppUser;
import com.football.football_app.model.Tournament;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TournamentRepository extends JpaRepository<Tournament, Long> {
    List<Tournament> findAllByOrganizer(AppUser org);

    Optional<Tournament> findByOrganizer(AppUser org);
}
