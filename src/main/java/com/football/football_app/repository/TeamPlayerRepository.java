package com.football.football_app.repository;

import com.football.football_app.model.TeamPlayer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeamPlayerRepository extends JpaRepository<TeamPlayer, Long> {
}
