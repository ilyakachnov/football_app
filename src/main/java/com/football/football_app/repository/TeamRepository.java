package com.football.football_app.repository;

import com.football.football_app.model.AppUser;
import com.football.football_app.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TeamRepository extends JpaRepository<Team, Long> {
    List<Team> findAllByOwner(AppUser currentUser);

    Optional<Team> findByOwnerAndId(AppUser currentUser, Long id);
}
