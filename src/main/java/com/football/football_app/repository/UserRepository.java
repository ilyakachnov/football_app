package com.football.football_app.repository;

import com.football.football_app.model.AppUser;
import com.football.football_app.model.RegistrationStatus;
import com.football.football_app.model.Team;
import com.football.football_app.model.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<AppUser, Long> {

    Optional<AppUser> findByEmail(String username);

    List<AppUser> findAllByUserRole(UserRole userRole);

    boolean existsByEmail(String email);

    boolean existsByPhone(Long phone);

    boolean existsByResetToken(String token);

    @Modifying(clearAutomatically = true)
    @Query("update AppUser user set user.status = :regStatus, user.activationUUID = null where user.id = :userId")
    void changeRegistrationStatus(@Param("userId") Long userId, @Param("regStatus") RegistrationStatus registrationStatus);

    @Query("select u from AppUser u inner join u.teamPlayers as ut inner join ut.team as utt where utt.owner = :owner and utt = :team")
    List<AppUser> findAllTeamPlayersByTeamAndOwner(@Param("team") Team team, @Param("owner") AppUser owner);

    @Query("select u from AppUser u inner join u.teamPlayers as ut inner join ut.team as utt where utt = :team")
    List<AppUser> findAllTeamPlayers(@Param("team") Team team);


}
