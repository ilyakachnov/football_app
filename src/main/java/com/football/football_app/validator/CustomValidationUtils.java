package com.football.football_app.validator;

import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.util.List;
import java.util.stream.Collectors;

public class CustomValidationUtils {
    public static List<String> groupValidationMessages(List<FieldError> fieldErrors) {
        return fieldErrors.stream().map(ObjectError::getDefaultMessage).collect(Collectors.toList());

    }
}
