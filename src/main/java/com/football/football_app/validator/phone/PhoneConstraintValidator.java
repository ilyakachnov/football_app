package com.football.football_app.validator.phone;

import com.football.football_app.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PhoneConstraintValidator implements ConstraintValidator<PhoneConstraint, Long> {
    private UserRepository userRepository;

    @Autowired
    public PhoneConstraintValidator(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void initialize(PhoneConstraint constraintAnnotation) {

    }

    @Override
    public boolean isValid(Long phone, ConstraintValidatorContext constraintValidatorContext) {
        return !userRepository.existsByPhone(phone);
    }
}
