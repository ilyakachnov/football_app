package com.football.football_app.controller;

import com.football.football_app.model.Tour;
import com.football.football_app.model.Tournament;
import com.football.football_app.security.AppUserPrincipal;
import com.football.football_app.service.TourService;
import com.football.football_app.service.TournamentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;

@Controller
@RequestMapping("/tournaments")
public class TourController {
    private final TourService tourService;
    private final TournamentService tournamentService;

    @Autowired
    public TourController(TourService tourService, TournamentService tournamentService) {
        this.tourService = tourService;
        this.tournamentService = tournamentService;
    }

    @GetMapping("{tId}/tours")
    public String showAllTours(Model model, @PathVariable("tId") Long id, Authentication authentication) {
        AppUserPrincipal principal = (AppUserPrincipal) authentication.getPrincipal();
        model.addAttribute("items", tourService.findAll(id, principal.getUser()));
        // TODO: 09.12.2019 try catch
        model.addAttribute("tournament", tournamentService.findTournament(id, principal.getUser()));
        return "tours/index";
    }

    @GetMapping("/{tId}/tours/create")
    public String showCreateForm(Model model, @PathVariable("tId") Long id, Authentication authentication) {
        AppUserPrincipal principal = (AppUserPrincipal) authentication.getPrincipal();
        model.addAttribute("tournament", tournamentService.findTournament(id, principal.getUser()));
        model.addAttribute("item", new Tour());
        return "tours/create";
    }

    @PostMapping("/{tId}/tours/save")
    public String saveTour(@ModelAttribute Tour tour, @PathVariable("tId") Long id, Authentication authentication) {
        AppUserPrincipal principal = (AppUserPrincipal) authentication.getPrincipal();
        tourService.save(tour, id, principal.getUser());
        return "redirect:";
    }

    @GetMapping("/{tId}/tours/{id}/edit")
    public String showEditForm(@PathVariable("tId") Long tournamentId, @PathVariable("id") Long id, Model model,
                               Authentication authentication) {
        AppUserPrincipal principal = (AppUserPrincipal) authentication.getPrincipal();
        Tour tour;
        Tournament tournament;
        try {
            tour = tourService.findTour(id, principal.getUser());
            tournament = tournamentService.findTournament(tournamentId, principal.getUser());
        } catch (EntityNotFoundException e) {
            return "error/404";
        }
        model.addAttribute("tournament", tournament);
        model.addAttribute("item", tour);
        return "tours/edit";
    }

    @PostMapping("/{tId}/tours/update")
    public String updateTour(@PathVariable("tId") Long tournamentId, Tour tourForm, Authentication authentication) {
        AppUserPrincipal principal = (AppUserPrincipal) authentication.getPrincipal();
        try {
            tourService.update(tourForm, principal.getUser());
        } catch (EntityNotFoundException e) {
            return "error/404";
        }
        return "redirect:";
    }


}
