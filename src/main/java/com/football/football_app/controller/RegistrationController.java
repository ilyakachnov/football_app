package com.football.football_app.controller;

import com.football.football_app.model.AppUser;
import com.football.football_app.service.RegistrationService;
import com.football.football_app.service.UserService;
import com.football.football_app.validator.CustomValidationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

@Controller
@RequestMapping("/registration")
public class RegistrationController {

    private final RegistrationService registrationService;
    private final UserService userService;
    @Value("${success.register}")
    private String successRegisterInfo;
    @Value("${success.activate}")
    private String successActivateInfo;

    @Autowired
    public RegistrationController(RegistrationService registrationService,
                                  UserService userService) {
        this.registrationService = registrationService;
        this.userService = userService;
    }

    @GetMapping
    public String registrationPage(Model model) {
        model.addAttribute(new AppUser());
        return "registration/register";
    }

    @PostMapping
    public String processRegistration(@ModelAttribute AppUser appUser, BindingResult result,
                                      RedirectAttributes redirectAttributes) {
//        if (result.hasErrors()) {
//            redirectAttributes.addFlashAttribute("errors", CustomValidationUtils.groupValidationMessages(result.getFieldErrors()));
//            redirectAttributes.addFlashAttribute("registerFormDto", appUser);
//            return "redirect:/registration";
//        }
        try {
            registrationService.saveUser(appUser);
        } catch (EntityNotFoundException e) {
            return "error/500";
        }
        redirectAttributes.addFlashAttribute("successMsg", successRegisterInfo);
        return "redirect:/login";
    }

    @GetMapping("/activation/{uuid}")
    public String activateUser(Authentication authentication, @PathVariable("uuid") String activateUuid, RedirectAttributes redirectAttributes) {
        try {
            registrationService.activateUser(userService.getCurrentUser(authentication).getId(), activateUuid);
        } catch (EntityNotFoundException e) {
            return "error/404";
        }
        redirectAttributes.addFlashAttribute("successMsg", successActivateInfo);
        return "redirect:/login?logout";
    }

}
