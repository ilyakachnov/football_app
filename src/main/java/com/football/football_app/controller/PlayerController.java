package com.football.football_app.controller;

import com.football.football_app.dto.TeamPlayerDTO;
import com.football.football_app.model.Team;
import com.football.football_app.security.AppUserPrincipal;
import com.football.football_app.service.PlayerService;
import com.football.football_app.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/allteams")
public class PlayerController {
    private final PlayerService playerService;
    private final TeamService teamService;

    @Autowired
    public PlayerController(PlayerService playerService, TeamService teamService) {
        this.playerService = playerService;
        this.teamService = teamService;
    }

    @GetMapping
    public String showAllTeamsToPlayer(Model model, Authentication authentication) {
        AppUserPrincipal principal = (AppUserPrincipal) authentication.getPrincipal();
        model.addAttribute("items", teamService.findAll(principal.getUser()));
        return "teams/index";
    }

    @GetMapping("/{id}/join")
    public String showJoinTeamForm(Model model, @PathVariable("id") Long teamId) {
        Team team;
        try {
            team = teamService.findTeam(teamId);
        } catch (Exception e) {
            return "error/404";
        }
        model.addAttribute("item", new TeamPlayerDTO(team.getId()));
        return "players/join";
    }

    @PostMapping("/join")
    public String joinTeam(@ModelAttribute TeamPlayerDTO teamPlayerDTO, Authentication authentication) {
        AppUserPrincipal principal = (AppUserPrincipal) authentication.getPrincipal();
        playerService.joinTeam(teamPlayerDTO, principal.getUser());
        return "redirect:";
    }

    @PostMapping("/{id}/leave")
    public String leaveTeam(@PathVariable("id") Long id, Authentication authentication) {
        AppUserPrincipal principal = (AppUserPrincipal) authentication.getPrincipal();
        playerService.leaveTeam(id, principal.getUser());
        return "redirect:/allteams";
    }
}
