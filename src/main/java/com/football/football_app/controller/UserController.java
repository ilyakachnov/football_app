package com.football.football_app.controller;

import com.football.football_app.model.AppUser;
import com.football.football_app.model.RegistrationStatus;
import com.football.football_app.security.AppUserPrincipal;
import com.football.football_app.service.RegistrationService;
import com.football.football_app.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.persistence.EntityNotFoundException;

@Controller
@RequestMapping("/users")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    private final UserService userService;
    private final RegistrationService registrationService;

    @Autowired
    public UserController(UserService userService, RegistrationService registrationService) {
        this.userService = userService;
        this.registrationService = registrationService;
    }

    @GetMapping
    public String showUsersPage(Model model) {
        model.addAttribute("users", userService.findAll());
        return "users/index";
    }

    @GetMapping("/create")
    public String showUserCreateForm(Model model) {
        model.addAttribute("user", new AppUser());
        return "users/create";
    }

    @GetMapping("/{id}/edit")
    public String showSingleUserPageForEdit(@PathVariable("id") Long id, Model model) {
        model.addAttribute("user", userService.getOne(id));
        return "users/edit";
    }

    @PostMapping("/save")
    public String saveUser(@ModelAttribute AppUser appUser, BindingResult result,
                           RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
//            redirectAttributes.addFlashAttribute("errors", CustomValidationUtils.groupValidationMessages(result.getFieldErrors()));
            redirectAttributes.addFlashAttribute("user", appUser);
            return "redirect:/users/";
        }
        userService.save(appUser);
        return "redirect:/users/";
    }

    @PostMapping("/update")
    public String updateUser(AppUser appUser) {
        try {
            userService.update(appUser);
        } catch (EntityNotFoundException e) {
            return "404";
        }
        return "redirect:/users/";
    }

    @PostMapping(value = "/change-status/{id}", params = "confirm")
    public String confirmUser(@PathVariable("id") Long id, Authentication authentication) {
        AppUserPrincipal userPrincipal = (AppUserPrincipal) authentication.getPrincipal();
        registrationService.changeUserRegistrationStatus(id, RegistrationStatus.CONFIRMED, userPrincipal.getUser());
        return "redirect:/users/" + id;
    }

    @PostMapping(value = "/change-status/{id}", params = "reject")
    public String rejectUser(@PathVariable("id") Long id, Authentication authentication) {
        AppUserPrincipal userPrincipal = (AppUserPrincipal) authentication.getPrincipal();
        registrationService.changeUserRegistrationStatus(id, RegistrationStatus.REJECTED, userPrincipal.getUser());
        return "redirect:/users/" + id;
    }

    @GetMapping("/{id}")
    public String showSingleUserPage(@PathVariable("id") Long id, Model model) {
        model.addAttribute("user", userService.getOne(id));
        return "users/info";
    }
}
