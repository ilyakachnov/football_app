package com.football.football_app.controller;

import com.football.football_app.model.Tournament;
import com.football.football_app.security.AppUserPrincipal;
import com.football.football_app.service.TournamentService;
import com.football.football_app.service.TournamentTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;

@Controller
@RequestMapping("/tournaments")
public class TournamentController {
    private final TournamentService tournamentService;
    private final TournamentTypeService tournamentTypeService;

    @Autowired
    public TournamentController(TournamentService tournamentService, TournamentTypeService tournamentTypeService) {
        this.tournamentService = tournamentService;
        this.tournamentTypeService = tournamentTypeService;
    }

    @GetMapping
    public String showTournaments(Model model, Authentication authentication) {
        AppUserPrincipal principal = (AppUserPrincipal) authentication.getPrincipal();
        model.addAttribute("items", tournamentService.findAll(principal.getUser()));
        return "tournaments/index";
    }

    @GetMapping("/create")
    public String showCreateForm(Model model) {
        model.addAttribute("item", new Tournament());
//        model.addAttribute("types", tournamentTypeService.findAll());
        return "tournaments/create";
    }

    @PostMapping("/save")
    public String saveTournament(@ModelAttribute Tournament tournament, Authentication authentication) {
        AppUserPrincipal principal = (AppUserPrincipal) authentication.getPrincipal();
        tournamentService.save(tournament, principal.getUser());
        // TODO: 09.12.2019 unique name
        return "redirect:/tournaments";
    }

    @GetMapping("/{id}/edit")
    public String showEditForm(@PathVariable("id") Long id, Model model, Authentication authentication) {
        AppUserPrincipal principal = (AppUserPrincipal) authentication.getPrincipal();
        Tournament tournament;
        try {
            tournament = tournamentService.findTournament(id, principal.getUser());
        } catch (EntityNotFoundException e) {
            return "error/404";
        }
        model.addAttribute("item", tournament);
//        model.addAttribute("types", tournamentTypeService.findAll());
        return "tournaments/edit";

    }

    @PostMapping("/update")
    public String updateTournament(@ModelAttribute Tournament tournament, Authentication authentication) {
        AppUserPrincipal principal = (AppUserPrincipal) authentication.getPrincipal();
        tournamentService.update(tournament, principal.getUser());
        // TODO: 09.12.2019 unique name
        return "redirect:/tournaments";
    }


}
