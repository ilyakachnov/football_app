package com.football.football_app.controller;

import com.football.football_app.model.League;
import com.football.football_app.service.LeagueService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;

@Controller
@RequestMapping("/leagues")
public class LeagueController {
    private final LeagueService leagueService;

    public LeagueController(LeagueService leagueService) {
        this.leagueService = leagueService;
    }

    @GetMapping
    public String showLeaguesPage(Model model) {
        model.addAttribute("leagues", leagueService.findAll());
        return "leagues/index";
    }

    @GetMapping("/create")
    public String showCreateForm(Model model) {
        model.addAttribute("item", new League());
        return "leagues/create";
    }

    @PostMapping("/save")
    public String saveLeague(@ModelAttribute League league) {
        leagueService.save(league);
        return "redirect:";
    }

    @GetMapping("/{id}/edit")
    public String showEditForm(@PathVariable("id") Long id, Model model) {
        League league;
        try {
            league = leagueService.findLeague(id);
        } catch (EntityNotFoundException e) {
            return "error/404";
        }
        model.addAttribute("item", league);
        return "leagues/edit";
    }

}
