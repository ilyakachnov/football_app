package com.football.football_app.controller;

import com.football.football_app.model.AppUser;
import com.football.football_app.security.AppUserPrincipal;
import com.football.football_app.service.RegistrationService;
import com.football.football_app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.persistence.EntityNotFoundException;

@Controller
@RequestMapping("/")
public class HomeController {

    private final UserService userService;
    private final RegistrationService registrationService;

    @Autowired
    public HomeController(UserService userService, RegistrationService registrationService) {
        this.userService = userService;
        this.registrationService = registrationService;
    }


    @GetMapping("/")
    public String showIndexPage(Authentication authentication, Model model) {
        try {
            model.addAttribute("user", userService.getCurrentUser(authentication));
        } catch (EntityNotFoundException e) {
            return "error/404";
        }
        return "users/info";
    }

    @GetMapping("/profile")
    public String showProfilePage(Authentication authentication, Model model) {
        try {
            model.addAttribute("user", userService.getCurrentUser(authentication));
        } catch (EntityNotFoundException e) {
            return "error/404";
        }
        return "profile";
    }

    @PostMapping("/profile/update")
    public String updateProfile(AppUser appUser) {
        try {
            userService.update(appUser);
        } catch (EntityNotFoundException e) {
            return "error/404";
        }
        return "redirect:/profile";
    }

    //    @PostMapping("profile/change-password")
//    public String changePassword(Authentication authentication, @RequestParam("new_password") String newPassword) {
//        // TODO: 05.08.2019 мб стоит сначала вводить страый пароль, и если он совпал то тогда менять
//        try {
//            userService.changePassword(authentication, newPassword);
//        } catch (EntityNotFoundException e) {
//            return "404";
//        }
//        return "redirect:/profile";
//    }
//
    @PostMapping("/remind")
    public String remindAboutUser(Authentication authentication) {
        AppUserPrincipal userPrincipal = (AppUserPrincipal) authentication.getPrincipal();
        Long userId = userPrincipal.getUser().getId();
        registrationService.remindAboutUser(userId);
        return "redirect:/";
    }
}
