package com.football.football_app.controller;

import com.football.football_app.model.AppUser;
import com.football.football_app.model.Team;
import com.football.football_app.security.AppUserPrincipal;
import com.football.football_app.service.LeagueService;
import com.football.football_app.service.PlayerService;
import com.football.football_app.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;

@Controller
@RequestMapping("/teams")
public class TeamController {
    private final TeamService teamService;
    private final LeagueService leagueService;
    private final PlayerService playerService;

    @Autowired
    public TeamController(TeamService teamService, LeagueService leagueService, PlayerService playerService) {
        this.teamService = teamService;
        this.leagueService = leagueService;
        this.playerService = playerService;
    }

    @GetMapping
    public String showTeams(Model model, Authentication authentication) {
        AppUserPrincipal principal = (AppUserPrincipal) authentication.getPrincipal();
        model.addAttribute("items", teamService.findAll(principal.getUser()));
        return "teams/index";
    }

    @GetMapping("{id}/players")
    public String showTeamPlayers(@PathVariable("id") Long id, Model model) {
        model.addAttribute("users", playerService.findAllByTeam(id));
//        AppUserPrincipal principal = (AppUserPrincipal) authentication.getPrincipal();
//        model.addAttribute("users", playerService.findUsersByTeam(id, principal.getUser()));
        return "players/index";
    }

    @GetMapping("/create")
    public String showCreateForm(Model model) {
        model.addAttribute("item", new Team());
        model.addAttribute("leagues", leagueService.findAll());
        return "teams/create";
    }

    @PostMapping("/save")
    public String saveTeam(@ModelAttribute Team team, Authentication authentication) {
        // TODO: 09.12.2019 if league not found
        AppUserPrincipal principal = (AppUserPrincipal) authentication.getPrincipal();
        teamService.save(team, principal.getUser());
        return "redirect:/teams";
    }

    @GetMapping("/{id}/edit")
    public String showEditForm(Authentication authentication, @PathVariable("id") Long id, Model model) {
        AppUserPrincipal appUserPrincipal = (AppUserPrincipal) authentication.getPrincipal();
        Team team;
        try {
            team = teamService.findTeamByOwner(id, appUserPrincipal.getUser());
        } catch (EntityNotFoundException e) {
            return "error/404";
        }
        model.addAttribute("team", team);
        return "teams/edit";
    }

    @PostMapping("/update")
    public String updateTeam(Authentication authentication, @ModelAttribute Team teamForm) {
        AppUserPrincipal appUserPrincipal = (AppUserPrincipal) authentication.getPrincipal();
        try {
            teamService.update(teamForm, appUserPrincipal.getUser());
        } catch (EntityNotFoundException e) {
            return "error/404";
        }

        return "redirect:/teams";
    }

}
