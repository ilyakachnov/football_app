package com.football.football_app.controller;

import com.football.football_app.model.Stadium;
import com.football.football_app.service.StadiumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;

@Controller
@RequestMapping("/stadiums")
public class StadiumController {
    private StadiumService stadiumService;

    @Autowired
    public StadiumController(StadiumService stadiumService) {
        this.stadiumService = stadiumService;
    }

    @GetMapping
    public String showStadiums(Model model) {
        model.addAttribute("items", stadiumService.findAllStadiums());
        return "stadiums/index";
    }

    @GetMapping("/create")
    public String showCreateForm(Model model) {
        model.addAttribute("item", new Stadium());
        return "stadiums/create";
    }

    @PostMapping("/save")
    public String saveStadium(@ModelAttribute Stadium stadiumForm) {
        stadiumService.saveStadium(stadiumForm);
        return "redirect:";
    }

    @GetMapping("/{id}/edit")
    public String showEditForm(@PathVariable("id") Long id, Model model) {
        Stadium stadiumForm;
        try {
            stadiumForm = stadiumService.findStadium(id);
        } catch (EntityNotFoundException e) {
            return "error/404";
        }
        model.addAttribute("item", stadiumForm);
        return "stadiums/edit";
    }

    @PostMapping("/update")
    public String updateStadium(@ModelAttribute Stadium stadiumForm){
        stadiumService.updateStadium(stadiumForm);
        return "redirect:";
    }
}
