package com.football.football_app.email;

import com.football.football_app.model.AppUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CustomMailSender {
    private static final Logger logger = LoggerFactory.getLogger(CustomMailSender.class);
    // TODO: 05.08.2019 set from
    @Value("${spring.mail.username}")
    protected String from;
    protected String subject;

    private final JavaMailSender javaMailSender;

    @Autowired
    public CustomMailSender(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Async
    public void send(List<AppUser> recipients, String message, String subject) {
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setTo(recipients.stream().map(AppUser::getEmail).toArray(String[]::new));
            messageHelper.setSubject(subject);
            messageHelper.setText(message, true);
            messageHelper.setFrom(from, "Футбол");
        };
        try {
            javaMailSender.send(messagePreparator);
        } catch (Exception e) {
            logger.error("Error while sending message", e);
        }
    }
}
