package com.football.football_app.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table
public class Stadium {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String address;

    private String name;

    public Long getId() {
        return id;
    }

    public Stadium setId(Long id) {
        this.id = id;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public Stadium setAddress(String address) {
        this.address = address;
        return this;
    }

    public String getName() {
        return name;
    }

    public Stadium setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Stadium stadium = (Stadium) o;
        return id.equals(stadium.id) &&
                address.equals(stadium.address) &&
                name.equals(stadium.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, address, name);
    }
}
