package com.football.football_app.model;

import com.football.football_app.validator.email.EmailConstraint;
import com.football.football_app.validator.phone.PhoneConstraint;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.*;

@Entity
@Table
public class AppUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

//    @NotBlank
    @Column
    private String name;

//    @NotBlank
    @Column
    private String surname;

    @Column
    private String patronymic;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

//    @PhoneConstraint(message = "{user.phone.not-unique}")
//    @NotNull
    @Column(unique = true, nullable = false)
    private Long phone;

//    @EmailConstraint(message = "{user.email.not-unique}")
//    @Email
//    @NotBlank
    @Column(nullable = false, unique = true, length = 60)
    private String email;

//    @NotBlank
    @Column(length = 60, nullable = false)
    private String password;

    /**
     * Токен для регенерации пароля
     */
    @Column
    private String resetToken;

    @Enumerated(EnumType.STRING)
    private RegistrationStatus status = RegistrationStatus.NOT_VIEWED;

    @Column(length = 40)
    private String activationUUID;

    @OneToMany(mappedBy = "owner")
    private List<Team> ownedTeams = new ArrayList<>();

    @OneToMany(mappedBy = "player", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<TeamPlayer> teamPlayers;

    @Enumerated(EnumType.STRING)
    private UserRole userRole;

    public AppUser() {
    }

    public Long getId() {
        return id;
    }

    public AppUser setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public AppUser setName(String name) {
        this.name = name;
        return this;
    }

    public String getSurname() {
        return surname;
    }

    public AppUser setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public AppUser setPatronymic(String patronymic) {
        this.patronymic = patronymic;
        return this;
    }

    public Date getBirthday() {
        return birthday;
    }

    public AppUser setBirthday(Date birthday) {
        this.birthday = birthday;
        return this;
    }

    public Long getPhone() {
        return phone;
    }

    public AppUser setPhone(Long phone) {
        this.phone = phone;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public AppUser setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public AppUser setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getResetToken() {
        return resetToken;
    }

    public AppUser setResetToken(String resetToken) {
        this.resetToken = resetToken;
        return this;
    }

    public RegistrationStatus getStatus() {
        return status;
    }

    public void setStatus(RegistrationStatus status) {
        this.status = status;
    }

    public String getActivationUUID() {
        return activationUUID;
    }

    public void setActivationUUID(String activationUUID) {
        this.activationUUID = activationUUID;
    }

    public List<Team> getOwnedTeams() {
        return ownedTeams;
    }

    public void setOwnedTeams(List<Team> ownedTeams) {
        this.ownedTeams = ownedTeams;
    }

    public Set<TeamPlayer> getTeamPlayers() {
        return teamPlayers;
    }

    public void setTeamPlayers(Set<TeamPlayer> teamPlayers) {
        this.teamPlayers = teamPlayers;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public Boolean isInTeam(Team team) {
        for (TeamPlayer tp : this.getTeamPlayers()) {
            if (tp.getTeam().equals(team)) {
                return true;
            }

        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AppUser that = (AppUser) o;
        return id.equals(that.id) &&
                phone.equals(that.phone) &&
                email.equals(that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, phone, email);
    }

    public String getTeamList() {
        String teamStr = "";
        for (TeamPlayer teamPlayer : this.getTeamPlayers()) {
            teamStr += teamPlayer.getTeam().getName() + ", ";
        }
        return teamStr;
    }
}
