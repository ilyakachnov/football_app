package com.football.football_app.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table
public class TeamPlayer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private AppUser player;

    @ManyToOne
    private Team team;

    @Enumerated(EnumType.STRING)
    private PlayerRole playerRole;

    public TeamPlayer() {
    }

    public Long getId() {
        return id;
    }

    public TeamPlayer setId(Long id) {
        this.id = id;
        return this;
    }

    public AppUser getPlayer() {
        return player;
    }

    public TeamPlayer setPlayer(AppUser player) {
        this.player = player;
        return this;
    }

    public Team getTeam() {
        return team;
    }

    public TeamPlayer setTeam(Team team) {
        this.team = team;
        return this;
    }

    public PlayerRole getPlayerRole() {
        return playerRole;
    }

    public TeamPlayer setPlayerRole(PlayerRole playerRole) {
        this.playerRole = playerRole;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TeamPlayer that = (TeamPlayer) o;
        return id.equals(that.id) &&
                player.equals(that.player) &&
                team.equals(that.team) &&
                playerRole == that.playerRole;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, player, team, playerRole);
    }
}
