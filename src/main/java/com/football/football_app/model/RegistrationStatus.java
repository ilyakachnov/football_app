package com.football.football_app.model;

public enum RegistrationStatus {
    ACTIVE("Активен"), CONFIRMED("Подтвержден"), REJECTED("Отклонен"), NOT_VIEWED("Не рассмотрен");

    String value;

    RegistrationStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
