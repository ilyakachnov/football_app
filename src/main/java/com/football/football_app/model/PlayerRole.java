package com.football.football_app.model;

public enum PlayerRole {

    FORWARD("Нападающий"), GOALKEEPER("Вратарь"),
    DEFENDER("Защитник"), MIDFIELDER("Полузащитник");

    private final String name;

    PlayerRole(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
