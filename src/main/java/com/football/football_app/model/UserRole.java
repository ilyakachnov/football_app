package com.football.football_app.model;

public enum UserRole {
    ADMIN, MANAGER, PLAYER, ORGANIZER
}
