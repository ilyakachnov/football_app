package com.football.football_app.dto;

import com.football.football_app.model.PlayerRole;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class TeamPlayerDTO {
    private PlayerRole playerRole;
    private String name;
    private String surname;
    private String patronymic;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

    private Long teamId;

    public TeamPlayerDTO(Long teamId) {
        this.teamId = teamId;
    }

    public TeamPlayerDTO() {
    }

    public PlayerRole getPlayerRole() {
        return playerRole;
    }

    public TeamPlayerDTO setPlayerRole(PlayerRole playerRole) {
        this.playerRole = playerRole;
        return this;
    }

    public Long getTeamId() {
        return teamId;
    }

    public TeamPlayerDTO setTeamId(Long teamId) {
        this.teamId = teamId;
        return this;
    }

    public String getName() {
        return name;
    }

    public TeamPlayerDTO setName(String name) {
        this.name = name;
        return this;
    }

    public String getSurname() {
        return surname;
    }

    public TeamPlayerDTO setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public TeamPlayerDTO setPatronymic(String patronymic) {
        this.patronymic = patronymic;
        return this;
    }

    public Date getBirthday() {
        return birthday;
    }

    public TeamPlayerDTO setBirthday(Date birthday) {
        this.birthday = birthday;
        return this;
    }
}
