package com.football.football_app.service;

import com.football.football_app.model.AppUser;
import com.football.football_app.model.Tournament;

import javax.persistence.EntityNotFoundException;
import java.util.List;

public interface TournamentService {
    List<Tournament> findAll(AppUser appUser);

    void save(Tournament tournament, AppUser appUser);

    void update(Tournament tournament, AppUser appUser) throws EntityNotFoundException;

    Tournament findTournament(Long id, AppUser appUser) throws EntityNotFoundException;
}
