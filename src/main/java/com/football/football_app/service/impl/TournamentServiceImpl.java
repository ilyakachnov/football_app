package com.football.football_app.service.impl;

import com.football.football_app.model.AppUser;
import com.football.football_app.model.Tournament;
import com.football.football_app.model.UserRole;
import com.football.football_app.repository.TournamentRepository;
import com.football.football_app.service.TournamentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class TournamentServiceImpl implements TournamentService {

    private final TournamentRepository tournamentRepository;

    @Autowired
    public TournamentServiceImpl(TournamentRepository tournamentRepository) {
        this.tournamentRepository = tournamentRepository;
    }

    @Override
    public List<Tournament> findAll(AppUser appUser) {
        if (appUser.getUserRole().equals(UserRole.ORGANIZER)) {
            return tournamentRepository.findAllByOrganizer(appUser);
        }
        return tournamentRepository.findAll();
    }

    @Override
    public void save(Tournament tournament, AppUser appUser) {
        tournament.setOrganizer(appUser);
        tournamentRepository.save(tournament);
    }

    @Override
    public void update(Tournament tournamentForm, AppUser appUser) throws EntityNotFoundException {
        Tournament tournament = findTournament(tournamentForm.getId(), appUser);
        // TODO: 12.12.2019 редактирование до начала турнира!
        tournament.setName(tournamentForm.getName());
        tournament.setStartDate(tournamentForm.getStartDate());
        tournament.setEndDate(tournamentForm.getEndDate());
        tournament.setTournamentType(tournamentForm.getTournamentType());
        tournament.setTypeName(tournamentForm.getTypeName());
        tournamentRepository.save(tournament);
    }

    @Override
    public Tournament findTournament(Long id, AppUser appUser) throws EntityNotFoundException {
        if (appUser.getUserRole().equals(UserRole.ORGANIZER)) {
            return tournamentRepository.findByOrganizer(appUser).orElseThrow(() -> new EntityNotFoundException("Tournament not found"));
        }
        return tournamentRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Tournament not found"));
    }
}
