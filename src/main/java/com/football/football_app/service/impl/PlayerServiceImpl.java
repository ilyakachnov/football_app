package com.football.football_app.service.impl;

import com.football.football_app.dto.TeamPlayerDTO;
import com.football.football_app.model.AppUser;
import com.football.football_app.model.PlayerRole;
import com.football.football_app.model.Team;
import com.football.football_app.model.TeamPlayer;
import com.football.football_app.repository.TeamPlayerRepository;
import com.football.football_app.repository.UserRepository;
import com.football.football_app.service.PlayerService;
import com.football.football_app.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.*;

@Service
public class PlayerServiceImpl implements PlayerService {
    private final UserRepository userRepository;
    private final TeamService teamService;
    private final TeamPlayerRepository teamPlayerRepository;

    @Autowired
    public PlayerServiceImpl(UserRepository userRepository, TeamService teamService, TeamPlayerRepository teamPlayerRepository) {
        this.userRepository = userRepository;
        this.teamService = teamService;
        this.teamPlayerRepository = teamPlayerRepository;
    }

    @Override
    @Transactional
    public void joinTeam(TeamPlayerDTO teamPlayerDTO, AppUser player) throws EntityNotFoundException {
        // TODO: 10.12.2019 if not found

        Team team = teamService.findTeam(teamPlayerDTO.getTeamId());
        player.setTeamPlayers(Collections.singleton(new TeamPlayer()
                .setPlayer(player)
                .setTeam(team)
                .setPlayerRole(teamPlayerDTO.getPlayerRole())));
        userRepository.save(player);
    }

    @Override
    @Transactional
    public void leaveTeam(Long id, AppUser player) throws EntityNotFoundException {
        Team team = teamService.findTeam(id);
        Set<TeamPlayer> teamPlayers = player.getTeamPlayers();
        Iterator<TeamPlayer> iterator = teamPlayers.iterator();
        while (iterator.hasNext()) {
            TeamPlayer teamPlayer = iterator.next();
            if (teamPlayer.getTeam().equals(team)) {
                iterator.remove();
                teamPlayerRepository.deleteById(teamPlayer.getId());
            }
        }
        player.setTeamPlayers(teamPlayers);
        userRepository.save(player);
    }

    @Override
    public List<TeamPlayerDTO> findUsersByTeam(Long id, AppUser appUser) throws EntityNotFoundException {
        Team team = teamService.findTeamByOwner(id, appUser);
        List<AppUser> players = userRepository.findAllTeamPlayersByTeamAndOwner(team, appUser);
        return getTeamPlayerDtoList(players, team);
    }

    @Override
    public List<TeamPlayerDTO> findAllByTeam(Long id) {
        Team team = teamService.findTeam(id);
        List<AppUser> players = userRepository.findAllTeamPlayers(team);
        return getTeamPlayerDtoList(players, team);

    }

    private List<TeamPlayerDTO> getTeamPlayerDtoList(List<AppUser> players, Team team) {
        List<TeamPlayerDTO> teamPlayerDTOS = new ArrayList<>();
        players.forEach(appUser1 -> teamPlayerDTOS.add(new TeamPlayerDTO()
                        .setName(appUser1.getName())
                        .setSurname(appUser1.getSurname())
                        .setBirthday(appUser1.getBirthday())
                        .setPatronymic(appUser1.getPatronymic())
                        .setPlayerRole(appUser1.getTeamPlayers().stream().filter(teamPlayer -> teamPlayer.getTeam().equals(team)).map(teamPlayer -> teamPlayer.getPlayerRole()).findFirst().orElse(PlayerRole.DEFENDER))
                )
        );
        return teamPlayerDTOS;
    }
}
