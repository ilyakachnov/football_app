package com.football.football_app.service.impl;

import com.football.football_app.configuration.CredentialsTemplateConfigurationProperties;
import com.football.football_app.configuration.MsgTemplateConfigurationProperties;
import com.football.football_app.configuration.UrlTemplateConfigurationProperties;
import com.football.football_app.model.AppUser;
import com.football.football_app.model.RegistrationStatus;
import com.football.football_app.repository.UserRepository;
import com.football.football_app.security.AppUserPrincipal;
import com.football.football_app.service.UserService;
import com.football.football_app.service.email.MailSenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.*;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCrypt;
    private final String userActivationMsgTemplate;
    private final String newUserMsgSubject;
    private final String userActivationUrl;
    private final String credentialsTemplate;
    private final MailSenderService mailSenderService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, BCryptPasswordEncoder bCrypt, MsgTemplateConfigurationProperties userActivationMsgTemplate, MsgTemplateConfigurationProperties newUserMsgSubject, UrlTemplateConfigurationProperties userActivationUrl,
                           CredentialsTemplateConfigurationProperties credentialsTemplate, MailSenderService mailSenderService) {
        this.userRepository = userRepository;
        this.bCrypt = bCrypt;
        this.userActivationMsgTemplate = userActivationMsgTemplate.getAccountActivation();
        this.newUserMsgSubject = newUserMsgSubject.getNewUserMsgSubject();
        this.userActivationUrl = userActivationUrl.getUserActivationUrl();
        this.credentialsTemplate = credentialsTemplate.getCredentials();
        this.mailSenderService = mailSenderService;
    }


    @Override
    public List<AppUser> findAll() {
        return userRepository.findAll();
    }

    @Override
    public AppUser getOne(Long id) {
        return userRepository.getOne(id);
    }

    @Override
    @Transactional
    public void save(AppUser appUser) {
        String rawPassword = "123456";
        appUser.setPassword(bCrypt.encode(rawPassword));
        appUser.setStatus(RegistrationStatus.ACTIVE);
        String activationUUID = UUID.randomUUID().toString();
        appUser.setActivationUUID(activationUUID);
        AppUser user = userRepository.save(appUser);

        Map<String, Object> params = new HashMap<>();
        params.put("header", credentialsTemplate);
        params.put("login", user.getEmail());
        params.put("password", rawPassword);
        params.put("text", userActivationMsgTemplate);
        params.put("url", getActivationUrl(user.getId(), activationUUID));
        mailSenderService.sendMail(Collections.singletonList(appUser),
                params, newUserMsgSubject, "email/new-user");
    }

    @Override
    @Transactional
    public AppUser update(AppUser appUser) throws EntityNotFoundException {
        AppUser generalUser = userRepository.findById(appUser.getId())
                .orElseThrow(() -> new EntityNotFoundException("User not found"));
        generalUser.setName(appUser.getName());
        generalUser.setPatronymic(appUser.getPatronymic());
        generalUser.setSurname(appUser.getSurname());
        generalUser.setBirthday(appUser.getBirthday());
        userRepository.save(generalUser);
        return appUser;
    }
//
//    @Override
//    public void delete(Long id) throws EntityNotFoundException {
//        if (!userRepository.existsById(id)) {
//            throw new EntityNotFoundException("User not found");
//        }
//        userRepository.deleteById(id);
//    }

    @Override
    public AppUser getCurrentUser(Authentication authentication) throws EntityNotFoundException {
        AppUserPrincipal userPrincipal = (AppUserPrincipal) authentication.getPrincipal();
        return userRepository.findByEmail(userPrincipal.getUsername()).orElseThrow(
                () -> new EntityNotFoundException("User not found")
        );
    }

//    @Override
//    @Transactional
//    public void changePassword(Authentication authentication, String newPassword) throws EntityNotFoundException {
//        GeneralUserPrincipal userPrincipal = (GeneralUserPrincipal) authentication.getPrincipal();
//        GeneralUser generalUser = userRepository.findByEmail(userPrincipal.getUsername()).orElseThrow(
//                () -> new EntityNotFoundException("User not found")
//        );
//        generalUser.setPassword(bCrypt.encode(newPassword));
//        userRepository.save(generalUser);
//    }

//    @Override
//    public boolean existsByResetToken(String token) {
//        return userRepository.existsByResetToken(token);
//    }

    private String getActivationUrl(long userId, String activationUUID) {
        return String.format(userActivationUrl, userId, activationUUID);
    }

}
