package com.football.football_app.service.impl;

import com.football.football_app.model.League;
import com.football.football_app.repository.LeagueRepository;
import com.football.football_app.service.LeagueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class LeagueServiceImpl implements LeagueService {
    private final LeagueRepository leagueRepository;

    @Autowired
    public LeagueServiceImpl(LeagueRepository leagueRepository) {
        this.leagueRepository = leagueRepository;
    }

    @Override
    public List<League> findAll() {
        return leagueRepository.findAll();
    }

    @Override
    public League findLeague(Long id) throws EntityNotFoundException {
        return leagueRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("League not found"));
    }

    @Override
    public void save(League league) {
        leagueRepository.save(league);
    }

    @Override
    public void update(League leagueForm) throws EntityNotFoundException {
        League league = leagueRepository.findById(leagueForm.getId())
                .orElseThrow(() -> new EntityNotFoundException("League not found"));
        league.setName(leagueForm.getName());
    }
}
