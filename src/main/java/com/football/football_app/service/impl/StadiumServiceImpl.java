package com.football.football_app.service.impl;

import com.football.football_app.model.Stadium;
import com.football.football_app.repository.StadiumRepository;
import com.football.football_app.service.StadiumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class StadiumServiceImpl implements StadiumService {
    private final StadiumRepository stadiumRepository;

    @Autowired
    public StadiumServiceImpl(StadiumRepository stadiumRepository) {
        this.stadiumRepository = stadiumRepository;
    }

    @Override
    public List<Stadium> findAllStadiums() {
        return stadiumRepository.findAll();
    }

    @Override
    public Stadium findStadium(Long id) throws EntityNotFoundException {
        return stadiumRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Stadium not found"));
    }

    @Override
    public void saveStadium(Stadium stadium) {
        stadiumRepository.save(stadium);
    }

    @Override
    public void updateStadium(Stadium stadiumForm) throws EntityNotFoundException {
        Stadium stadium = findStadium(stadiumForm.getId());
        stadium.setAddress(stadiumForm.getAddress()).setName(stadiumForm.getName());
        stadiumRepository.save(stadium);
    }
}
