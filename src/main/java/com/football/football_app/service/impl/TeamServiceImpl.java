package com.football.football_app.service.impl;

import com.football.football_app.model.AppUser;
import com.football.football_app.model.Team;
import com.football.football_app.model.UserRole;
import com.football.football_app.repository.TeamRepository;
import com.football.football_app.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class TeamServiceImpl implements TeamService {
    private final TeamRepository teamRepository;

    @Autowired
    public TeamServiceImpl(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    @Override
    public List<Team> findAll(AppUser currentUser) {
        if (currentUser.getUserRole().equals(UserRole.MANAGER)) {
            return teamRepository.findAllByOwner(currentUser);
        }
        return teamRepository.findAll();
    }

    @Override
    public Team findTeamByOwner(Long id, AppUser appUser) throws EntityNotFoundException {
        if (appUser.getUserRole().equals(UserRole.ADMIN)) {
            return teamRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Team not found"));
        }
        return teamRepository.findByOwnerAndId(appUser, id).orElseThrow(() -> new EntityNotFoundException("Team not found"));
    }

    @Override
    public void save(Team team, AppUser appUser) {
        // TODO: 09.12.2019 if league not found
        team.setOwner(appUser);
        teamRepository.save(team);
    }

    @Override
    public void update(Team teamForm, AppUser appUser) throws EntityNotFoundException {
        Team team;
        if (appUser.getUserRole().equals(UserRole.ADMIN)) {
            team = teamRepository.findById(teamForm.getId()).orElseThrow(() -> new EntityNotFoundException("Team not found"));
        } else {
            team = teamRepository.findByOwnerAndId(appUser, teamForm.getId()).orElseThrow(() -> new EntityNotFoundException("Team not found"));
        }
        team.setName(teamForm.getName());
        team.setDescription(teamForm.getDescription());
        teamRepository.save(team);

    }

    @Override
    public Team findTeam(Long id) {
        return teamRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Team not found"));
    }
}
