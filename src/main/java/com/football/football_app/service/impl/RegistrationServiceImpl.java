package com.football.football_app.service.impl;

import com.football.football_app.configuration.MsgTemplateConfigurationProperties;
import com.football.football_app.configuration.UrlTemplateConfigurationProperties;
import com.football.football_app.model.AppUser;
import com.football.football_app.model.RegistrationStatus;
import com.football.football_app.model.UserRole;
import com.football.football_app.repository.UserRepository;
import com.football.football_app.service.RegistrationService;
import com.football.football_app.service.email.MailSenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.*;

@Service
public class RegistrationServiceImpl implements RegistrationService {

    private static final BCryptPasswordEncoder bCrypt = new BCryptPasswordEncoder();
    private final UserRepository userRepository;
    private final String newUserNotificationMsgTemplate;
    private final String userActivationMsgTemplate;
    private final String newUserMsgSubject;
    private final MailSenderService mailSenderService;
    private final String userProfileUrl;
    private final String userActivationUrl;

    @Autowired
    public RegistrationServiceImpl(
            UserRepository userRepository,
            MailSenderService mailSenderService,
            MsgTemplateConfigurationProperties msgTemplateConfigurationProperties,
            UrlTemplateConfigurationProperties urlTemplateConfigurationProperties
    ) {
        this.userRepository = userRepository;
        this.mailSenderService = mailSenderService;
        this.newUserNotificationMsgTemplate = msgTemplateConfigurationProperties.getNewUserNotification();
        this.userActivationMsgTemplate = msgTemplateConfigurationProperties.getAccountActivation();
        this.newUserMsgSubject = msgTemplateConfigurationProperties.getNewUserMsgSubject();
        this.userProfileUrl = urlTemplateConfigurationProperties.getUserProfileUrl();
        this.userActivationUrl = urlTemplateConfigurationProperties.getUserActivationUrl();
    }

    @Override
    @Transactional
    public void saveUser(AppUser user) throws EntityNotFoundException {
        user.setPassword(bCrypt.encode(user.getPassword()));
        userRepository.save(user);
        remindAboutUser(user.getId());
    }

    @Override
    public void remindAboutUser(Long userId) {
        // TODO: 07.08.2019 from position
        List<AppUser> admins = userRepository.findAllByUserRole(UserRole.ADMIN);
        Map<String, Object> params = new HashMap<>();
        params.put("text", newUserNotificationMsgTemplate);
        params.put("url", getUrlForUserProfile(userId));
        mailSenderService.sendMail(admins, params, newUserMsgSubject, "email/message");
    }

    @Override
    @Transactional
    public void changeUserRegistrationStatus(long userId, RegistrationStatus registrationStatus, AppUser currUser) {
        // TODO: 07.08.2019 проверка через position
        if (currUser.getUserRole() != UserRole.ADMIN) {
            return;
        }
        if (registrationStatus == RegistrationStatus.ACTIVE) {
            return;
        }
        AppUser generalUser = userRepository.getOne(userId);
        generalUser.setStatus(registrationStatus);
        if (registrationStatus == RegistrationStatus.CONFIRMED) {
            String activationUUID = UUID.randomUUID().toString();
            generalUser.setActivationUUID(activationUUID);
            Map<String, Object> params = new HashMap<>();
            params.put("text", userActivationMsgTemplate);
            params.put("url", getActivationUrl(activationUUID));
            mailSenderService.sendMail(Collections.singletonList(generalUser),
                    params, newUserMsgSubject, "email/message");
        }
        userRepository.save(generalUser);
    }

    @Override
    @Transactional
    public void activateUser(long userId, String activationUUID) throws EntityNotFoundException {
        Optional<AppUser> appUser = userRepository.findById(userId);
        if (!appUser.isPresent()) {
            throw new EntityNotFoundException("User with id = " + userId + " not found");
        }
        AppUser appUser1 = appUser.get();
        if (appUser1.getActivationUUID() == null || !appUser1.getActivationUUID().equals(activationUUID)) {
            String errorMessage = String.format(
                    "Wrong UUID for activation user: %s, wrong UUID: %s", appUser1, activationUUID
            );
            throw new EntityNotFoundException(errorMessage);
        }
        appUser1.setStatus(RegistrationStatus.ACTIVE);
        userRepository.changeRegistrationStatus(userId, RegistrationStatus.ACTIVE);
    }

    private String getUrlForUserProfile(Long userId) {
        return String.format(userProfileUrl, userId);
    }

    private String getActivationUrl(String activationUUID) {
        return String.format(userActivationUrl, activationUUID);
    }

}
