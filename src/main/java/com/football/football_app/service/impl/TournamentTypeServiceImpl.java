package com.football.football_app.service.impl;

import com.football.football_app.model.TournamentType;
import com.football.football_app.repository.TournamentTypeRepository;
import com.football.football_app.service.TournamentTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TournamentTypeServiceImpl implements TournamentTypeService {
    private final TournamentTypeRepository repository;

    @Autowired
    public TournamentTypeServiceImpl(TournamentTypeRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<TournamentType> findAll() {
        return repository.findAll();
    }
}
