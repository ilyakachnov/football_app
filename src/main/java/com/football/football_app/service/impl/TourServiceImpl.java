package com.football.football_app.service.impl;

import com.football.football_app.model.AppUser;
import com.football.football_app.model.Tour;
import com.football.football_app.model.Tournament;
import com.football.football_app.model.UserRole;
import com.football.football_app.repository.TourRepository;
import com.football.football_app.service.TourService;
import com.football.football_app.service.TournamentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class TourServiceImpl implements TourService {
    private final TourRepository tourRepository;
    private final TournamentService tournamentService;

    @Autowired
    public TourServiceImpl(TourRepository tourRepository, TournamentService tournamentService) {
        this.tourRepository = tourRepository;
        this.tournamentService = tournamentService;
    }

    @Override
    public List<Tour> findAll(Long tournamentId, AppUser appUser) {
        Tournament tournament = tournamentService.findTournament(tournamentId, appUser);
        return tourRepository.findAllByTournament(tournament);
    }

    @Override
    public Tour findTour(Long id, AppUser appUser) throws EntityNotFoundException {
        if (appUser.getUserRole().equals(UserRole.ADMIN)) {
            return tourRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Tour not found"));
        }
        return tourRepository.findTourByOrganizer(id, appUser).orElseThrow(() -> new EntityNotFoundException("Tour not found"));
    }

    @Override
    public void save(Tour tour, Long tournamentId, AppUser appUser) {
        // TODO: 09.12.2019 if not found
        Tournament tournament = tournamentService.findTournament(tournamentId, appUser);
        tour.setTournament(tournament);
        tourRepository.save(tour);
    }


    @Override
    public void update(Tour tourForm, AppUser appUser) throws EntityNotFoundException {
        Tour tour = findTour(tourForm.getId(), appUser);
        tour.setName(tourForm.getName());
        tour.setStartDate(tourForm.getStartDate());
        tour.setEndDate(tourForm.getEndDate());
        tourRepository.save(tour);
    }
}
