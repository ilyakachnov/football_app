package com.football.football_app.service;

import com.football.football_app.model.TournamentType;

import java.util.List;

public interface TournamentTypeService {
    List<TournamentType> findAll();
}
