package com.football.football_app.service;

import com.football.football_app.model.Stadium;

import javax.persistence.EntityNotFoundException;
import java.util.List;

public interface StadiumService {
    List<Stadium> findAllStadiums();

    Stadium findStadium(Long id) throws EntityNotFoundException;

    void saveStadium(Stadium stadium);

    void updateStadium(Stadium stadium) throws EntityNotFoundException;

}
