package com.football.football_app.service;

import com.football.football_app.model.AppUser;
import com.football.football_app.model.Team;

import javax.persistence.EntityNotFoundException;
import java.util.List;

public interface TeamService {
    List<Team> findAll(AppUser currentUser);

    void save(Team team, AppUser appUser);

    void update(Team teamForm, AppUser appUser) throws EntityNotFoundException;

    Team findTeam(Long id);

    Team findTeamByOwner(Long id, AppUser appUser) throws EntityNotFoundException;
}
