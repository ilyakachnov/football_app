package com.football.football_app.service.email;

import com.football.football_app.email.CustomMailSender;
import com.football.football_app.model.AppUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.List;
import java.util.Map;

@Service
public class MailSenderService {
    private final CustomMailSender mailSender;
    private final TemplateEngine templateEngine;

    @Autowired
    public MailSenderService(CustomMailSender mailSender, TemplateEngine templateEngine) {
        this.mailSender = mailSender;
        this.templateEngine = templateEngine;
    }

    public void sendMail(List<AppUser> recipients, Map<String, Object> parameters, String subject, String template) {
        mailSender.send(recipients, generateMessage(parameters, template), subject);
    }

    private String generateMessage(Map<String, Object> parameters, String template) {
        Context context = new Context();
        context.setVariables(parameters);
        if (template == null) {
            template = "email/message";
        }
        return templateEngine.process(template, context);
    }

}
