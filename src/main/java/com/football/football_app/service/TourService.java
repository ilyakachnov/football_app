package com.football.football_app.service;

import com.football.football_app.model.AppUser;
import com.football.football_app.model.Tour;

import javax.persistence.EntityNotFoundException;
import java.util.List;

public interface TourService {
    List<Tour> findAll(Long tournamentId, AppUser appUser);

    void save(Tour tour, Long tournamentId, AppUser appUser);

    void update(Tour tour, AppUser appUser) throws EntityNotFoundException;

    Tour findTour(Long id, AppUser appUser) throws EntityNotFoundException;
}
