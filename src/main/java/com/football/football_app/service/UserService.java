package com.football.football_app.service;

import com.football.football_app.model.AppUser;
import org.springframework.security.core.Authentication;

import javax.persistence.EntityNotFoundException;
import java.util.List;

public interface UserService {
    List<AppUser> findAll();

    AppUser getOne(Long id);

    void save(AppUser user);

    AppUser update(AppUser user) throws EntityNotFoundException;

    AppUser getCurrentUser(Authentication authentication) throws EntityNotFoundException;
//
//    void changePassword(Authentication authentication, String newPassword) throws EntityNotFoundException;
//
//    boolean existsByResetToken(String token);

//    void delete(Long id) throws EntityNotFoundException;
}
