package com.football.football_app.service;

import com.football.football_app.model.AppUser;
import com.football.football_app.model.RegistrationStatus;

import javax.persistence.EntityNotFoundException;

public interface RegistrationService {

    void saveUser(AppUser appUser) throws EntityNotFoundException;

    void remindAboutUser(Long userId);

    void changeUserRegistrationStatus(long userId, RegistrationStatus registrationStatus, AppUser currUser) ;

    void activateUser(long userId, String activationUUID) throws  EntityNotFoundException;
}
