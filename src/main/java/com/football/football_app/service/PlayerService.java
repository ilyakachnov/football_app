package com.football.football_app.service;

import com.football.football_app.dto.TeamPlayerDTO;
import com.football.football_app.model.AppUser;

import javax.persistence.EntityNotFoundException;
import java.util.List;

public interface PlayerService {
    void joinTeam(TeamPlayerDTO teamPlayerDTO, AppUser player) throws EntityNotFoundException;

    void leaveTeam(Long id, AppUser player) throws EntityNotFoundException;

    List<TeamPlayerDTO> findUsersByTeam(Long id, AppUser appUser);

    List<TeamPlayerDTO> findAllByTeam(Long id);
}
