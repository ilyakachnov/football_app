package com.football.football_app.service;

import com.football.football_app.model.League;

import javax.persistence.EntityNotFoundException;
import java.util.List;

public interface LeagueService {
    List<League> findAll();

    void save(League league);

    void update(League league) throws EntityNotFoundException;

    League findLeague(Long id) throws EntityNotFoundException;
}
