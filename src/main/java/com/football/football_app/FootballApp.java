package com.football.football_app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class FootballApp {

    public static void main(String[] args) {

        SpringApplication.run(FootballApp.class, args);
    }
}
