package com.football.football_app.helper;

import java.util.Arrays;

public class UrlHelper {
    private static final String[] patterns = {"/users"};

    public static boolean isContained(String uri) {
        return Arrays.stream(patterns).anyMatch(str -> str.equals(uri) || uri.contains(str));
    }

    public static boolean isUsersPage(String uri) {
        String usersStr = "/users";
        return usersStr.equals(uri) || uri.contains(usersStr);
    }
}
